extends Control

var games = []
var current_game = null

var IDLE_TIME = 10 * 1000

var Launcher = preload("res://Launcher.tscn")

var STATS_PATH = "user://stats.save"

func global_path(path):
	return ProjectSettings.globalize_path(path)

func save_stats(gs):
	var stats = FileAccess.open(STATS_PATH, FileAccess.WRITE)
	for g in gs:
		stats.store_line(JSON.stringify({name = g.name, plays = g.plays}))

func load_stats(gs):
	if not FileAccess.file_exists(STATS_PATH):
		save_stats(gs)
		return

	var stats = FileAccess.open(STATS_PATH, FileAccess.READ)
	while stats.get_position() < stats.get_length():
		var json_string = stats.get_line()
		var json = JSON.new()
		var parse_result = json.parse(json_string)
		if not parse_result == OK:
			print("JSON Parse Error: ", json.get_error_message(), " in ", json_string, " at line ", json.get_error_line())
			continue
		for g in gs:
			if g.name == json.data.name:
				g.plays = json.data.plays

	print(gs)

func run_game(n):
	if current_game != null:
		OS.kill(current_game)

	var g = null
	for ga in games:
		if ga.name == n:
			g = ga
	focus_workspace(2)
	current_game = OS.create_process(g.executable, [])
	g.plays += 1
	save_stats(games)


func create_menu(gs):
	for g in gs:
		var item = Launcher.instantiate()
		item.data = g
		item.connect("run", run_game)
		$PanelContainer/VBoxContainer.add_child(item)
		item.grab_focus()

func get_games(path):
	var gs = []
	var dir = DirAccess.open(path)
	if dir:
		dir.list_dir_begin()
		var filename = dir.get_next()
		while filename != "":
			if dir.current_is_dir():
				var dirpath = path + "/" + filename + "/"
				print("Found directory: " + filename)
				var json = read_json(dirpath + "meta.json")

				var data = {
					name = json.name,
					icon = dirpath + json.icon,
					year = json.year,
					plays = 0,
					executable = global_path(dirpath + json.exe)
				}

				gs.push_back(data)

			else:
				print("Found file: " + filename)
			filename = dir.get_next()
	else:
		print("An error occurred when trying to access the path.")

	gs.sort_custom(func(a, b): return a.year > b.year)
	return gs

func read_json(path):
	var file = FileAccess.open(path, FileAccess.READ)
	var contents = file.get_as_text()
	file.close()
	return JSON.parse_string(contents)

func focus_workspace(i = 1):
	OS.execute("i3-msg", ["workspace", str(i)])

func check_status():
	if current_game != null:
		if OS.is_process_running(current_game):
			if idle_millis() > IDLE_TIME:
				focus_workspace()
				OS.kill(current_game)
				# OS.execute("killall", ["godot"])
				current_game = null
		else:
			focus_workspace()
			current_game = null
	else:
		focus_workspace()

func idle_millis():
	var output = []
	var e = OS.execute("xprintidle", [], output)
	if e == 0:
		return int(output[0])
	else:
		return 0


func _ready():
	print(global_path(STATS_PATH))
	games = get_games("res://games")
	load_stats(games)
	create_menu(games)
	$Timer.connect("timeout", check_status)
	pass

func _process(delta):
	pass
