extends Button

var data = null

signal run(e)

func _ready():
	$HBoxContainer/Label.text = " " + data.name
	# print(data.icon)
	$HBoxContainer/TextureRect.texture = load(data.icon)
	connect("pressed", func():emit_signal("run", data.name))
	connect("mouse_entered", grab_focus)
	pass
